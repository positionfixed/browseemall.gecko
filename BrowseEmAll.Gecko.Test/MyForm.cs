﻿using BrowseEmAll.Gecko.Core;
using BrowseEmAll.Gecko.Core.Events;
using BrowseEmAll.Gecko.Winforms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrowseEmAll.Gecko.Test
{
    class MyForm : Form
    {
        private TabControl m_tabControl;
        private TextBox urlbox;

        public MyForm()
        {
            this.Width = 1024;
            this.Height = 768;

            m_tabControl = new TabControl();
            m_tabControl.Dock = DockStyle.Fill;

            AddBrowser();

            Controls.Add(m_tabControl);

            m_tabControl.ControlRemoved += delegate 
            {
                if (m_tabControl.TabCount == 1)
                {
                    AddBrowser();
                }
            };

        }

        protected GeckoWebBrowser AddBrowser()
        {
            var tabPage = new TabPage();
            tabPage.Text = "blank";

            var browser = new GeckoWebBrowser();

            browser.Navigate("http://www.google.de");
            browser.Dock = DockStyle.Fill;
            tabPage.DockPadding.Top = 25;
            tabPage.Dock = DockStyle.Fill;

            AddToolbarAndBrowserToTab(tabPage, browser);

            m_tabControl.TabPages.Add(tabPage);
            tabPage.Show();
            m_tabControl.SelectedTab = tabPage;

            return browser;
        }

        protected void AddToolbarAndBrowserToTab(TabPage tabPage, GeckoWebBrowser browser)
        {
            urlbox = new TextBox();
            urlbox.Top = 0;
            urlbox.Width = 200;

            Button nav = new Button();
            nav.Text = "Go";
            nav.Left = urlbox.Width;

            Button stop = new Button();
            stop.Text = "Stop";
            stop.Left = nav.Left + nav.Width;

            Button executeJS = new Button();
            executeJS.Text = "Execute JS";
            executeJS.Left = stop.Left + stop.Width;

            Button screenshot = new Button();
            screenshot.Text = "Screenshot";
            screenshot.Left = executeJS.Left + executeJS.Width;

            executeJS.Click += (s, e) =>
            {
                string js = "return 7+3;";

                string result = browser.ExecuteJavaScript(js);
                MessageBox.Show("JavaScript returned " + result + " for: " + js);

                result = browser.ExecuteJavaScript("return 7+4;");
                MessageBox.Show("JavaScript returned " + result + " for: " + "return 7+4;");
            };

            screenshot.Click += (s, e) =>
            {
                byte[] mBytes = browser.TakePNGScreenshot();

                MessageBox.Show("The page screenshot size is " + mBytes.Length);

            };

            nav.Click += delegate
            {
                // use javascript to warn if url box is empty.
                if (string.IsNullOrEmpty(urlbox.Text.Trim()))
                    browser.Navigate("javascript:alert('hey try typing a url!');");

                browser.Navigate(urlbox.Text);
            };

            stop.Click += delegate { browser.Stop(); };

            AddEventsToBrowser(tabPage,browser);

            tabPage.Controls.Add(urlbox);
            tabPage.Controls.Add(nav);
            tabPage.Controls.Add(stop);
            tabPage.Controls.Add(browser);
            tabPage.Controls.Add(executeJS);
            tabPage.Controls.Add(screenshot);
        }

        private void AddEventsToBrowser(TabPage tabPage, GeckoWebBrowser browser)
        {
            browser.Navigating += (s, e) =>
            {
                Console.WriteLine("Navigating: url: " + e.Uri);
            };

            browser.Navigated += (s, e) =>
            {
                urlbox.Text = e.Uri.ToString();
                Console.WriteLine("Navigated: url: " + e.Uri + ", errorPage: " + e.IsErrorPage);
            };

            browser.DocumentCompleted += (s, e) =>
            {
                Console.WriteLine("DocumentCompleted: url: " + e.Uri);
            };

            browser.DocumentTitleChanged += (s, e) =>
            {
                tabPage.Text = browser.DocumentTitle;
            };

            browser.NSSError += (s, e) =>
            {
                // This event can be used to handle SSL errors (and override them)

                int flags = 0;
                flags |= nsICertOverrideServiceConsts.ERROR_UNTRUSTED;
                flags |= nsICertOverrideServiceConsts.ERROR_MISMATCH;
                flags |= nsICertOverrideServiceConsts.ERROR_TIME;

                if (!CertOverrideService.HasMatchingOverride(e.Uri, e.SSLStatus.GetServerCertAttribute()))
                {
                    CertOverrideService.RememberValidityOverride(e.Uri, e.SSLStatus.GetServerCertAttribute(), flags);

                    string refUrl = browser.LastNavigatedUrl;
                    browser.Navigate(refUrl, GeckoLoadFlags.FirstLoad);
                    e.Handled = true;
                }
            };
        }
    }

}
