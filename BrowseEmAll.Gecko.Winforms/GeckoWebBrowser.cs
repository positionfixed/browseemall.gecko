﻿#region ***** BEGIN LICENSE BLOCK *****
/* Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Skybound Software code.
 *
 * The Initial Developer of the Original Code is Skybound Software.
 * Portions created by the Initial Developer are Copyright (C) 2008-2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 */
#endregion END LICENSE BLOCK

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Text;
using BrowseEmAll.Gecko.Core.Events;
using BrowseEmAll.Gecko.Core.Interop;
using BrowseEmAll.Gecko.Core.Net;
using BrowseEmAll.Gecko.Core.IO;
using BrowseEmAll.Gecko.Core;
using BrowseEmAll.Gecko.Winforms.Interop;
using BrowseEmAll.Gecko.Core.DOM;

namespace BrowseEmAll.Gecko.Winforms
{
	/// <summary>
	/// A Gecko-based web browser.
	/// </summary>
	public partial class GeckoWebBrowser :
 		IGeckoWebBrowser, 
		nsIWebBrowserChrome,
		nsIContextMenuListener2,
		nsIWebProgressListener,
		nsIWebProgressListener2,
		nsIInterfaceRequestor,
		nsIEmbeddingSiteWindow,
		nsISHistoryListener,
		nsITooltipListener,
		nsISupportsWeakReference
	{
		/// <summary>
		/// Additional DOM message listeners
		/// </summary>
		Dictionary<string, Action<string>> _messageEventListeners = new Dictionary<string, Action<string>>();

		/// <summary>
		/// nsIWebBrowser instance
		/// </summary>
		nsIWebBrowser WebBrowser;

		/// <summary>
		/// nsIWebBrowser casted to nsIBaseWindow
		/// </summary>
		nsIBaseWindow BaseWindow;

		/// <summary>
		/// nsIWebBrowser casted no nsIWebNavigation
		/// </summary>
		nsIWebNavigation WebNav;

        nsICommandParams CommandParams;

		uint ChromeFlags;

		GeckoWindow _Window;
		
        GeckoDomDocument _Document;

		/// <summary>
		/// Initializes a new instance of <see cref="GeckoWebBrowser"/>.
		/// </summary>
		public GeckoWebBrowser()
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_Window != null)
					_Window.Dispose();
				_Window = null;
				if (_Document != null)
					_Document.Dispose();
				_Document = null;
			}
			base.Dispose(disposing);
		}

		protected virtual bool HasShutdownStarted()
		{
			return Environment.HasShutdownStarted || AppDomain.CurrentDomain.IsFinalizingForUnload();
		}

		public nsIWebBrowserFocus WebBrowserFocus
		{
			get; protected set;
		}
        
        /// <summary>
        /// Navigates to the specified URL.
        /// In order to find out when Navigate has finished attach a handler to NavigateFinishedNotifier.NavigateFinished.
        /// </summary>
        /// <param name="url">The url to navigate to.</param>
        public void Navigate(Uri url)
        {
            Navigate(url.AbsolutePath);
        }

		/// <summary>
		/// Navigates to the specified URL.
		/// In order to find out when Navigate has finished attach a handler to NavigateFinishedNotifier.NavigateFinished.
		/// </summary>
		/// <param name="url">The url to navigate to.</param>
		public void Navigate(string url)
		{
			Navigate(url, 0);
		}

		/// <summary>
		///  Navigates to the specified URL using the given load flags
		///  In order to find out when Navigate has finished attach a handler to NavigateFinishedNotifier.NavigateFinished.
		/// </summary>
		/// <param name="url">The url to navigate to.  If the url is empty or null, the browser does not navigate and the method returns false.</param>
		/// <param name="loadFlags">Flags which specify how the page is loaded.</param>
		/// <returns>true if Navigate started. false otherwise.</returns>
		public bool Navigate(string url, GeckoLoadFlags loadFlags)		
		{
			if (string.IsNullOrEmpty(url))
				return false;

            if (!url.Equals("about:blank"))
                LastNavigatedUrl = url;

            // added these from http://code.google.com/p/geckofx/issues/detail?id=5 so that it will work even if browser isn't currently shown
            if (!IsHandleCreated) CreateHandle(); 
			if (IsBusy) this.Stop();

			if (!IsHandleCreated)
				throw new InvalidOperationException("Cannot call Navigate() before the window handle is created.");

			nsIURI referrerUri = null;

			// We want Navigate() to return immediately and to fire events asynchronously. Howerver,
			// WebNav.LoadURI() may fire 'Navigating' event synchronously, so we call it asynchronously.
			// WebNav.LoadURI may throw exceptions for some inaccessable urls
			// (see https://bugzilla.mozilla.org/show_bug.cgi?id=995298), 
			// so we convert them into NavigationError events.
			BeginInvoke(new Action(() =>
			{
				if (IsDisposed)
					return;

				try
				{
					WebNav.LoadURI(
						url, (uint) loadFlags, referrerUri, null, null );
				}
				catch (COMException ce)
				{
					OnNavigationError(new GeckoNavigationErrorEventArgs(url, Window, ce.ErrorCode));
				}
				catch (Exception)
				{
					OnNavigationError(new GeckoNavigationErrorEventArgs(url, Window, GeckoError.NS_ERROR_UNEXPECTED));
				}
			}));

			return true;
		}
	
		/// <summary>
		/// Gets or sets whether all default items are removed from the standard context menu.
		/// </summary>
		[DefaultValue(false),Description("Removes default items from the standard context menu.  The ShowContextMenu event is still raised to add custom items.")]
		public bool NoDefaultContextMenu { get; set; }
		
		/// <summary>
		/// Gets the text displayed in the status bar.
		/// </summary>
		[Browsable(false), DefaultValue("")]
		public string StatusText { get; private set; }
		
		/// <summary>
		/// Gets the title of the document loaded into the web browser.
		/// </summary>
		[Browsable(false), DefaultValue("")]
		public string DocumentTitle { get; private set; }

        /// <summary>
        /// Gets the source of the document loaded into the web browser.
        /// </summary>
        [Browsable(false), DefaultValue("")]
        public string DocumentText
        {
            get
            {
                if (this.Document == null || this.Document.Body == null || this.Document.Body.Parent == null)
                    return string.Empty;

                return this.Document.Body.Parent.OuterHtml;
            }
        }

        /// <summary>
        /// Gets whether the browser may navigate back in the history.
        /// </summary>
        [BrowsableAttribute(false)]
		public bool CanGoBack { get; private set; }
		
		/// <summary>
		/// Gets whether the browser may navigate forward in the history.
		/// </summary>
		[BrowsableAttribute(false)]
		public bool CanGoForward { get; private set; }
		
		/// <summary>Raises the CanGoBackChanged or CanGoForwardChanged events when necessary.</summary>
		void UpdateCommandStatus()
		{
			bool canGoBack = false;
			bool canGoForward = false;
			if (WebNav != null)
			{
				canGoBack = WebNav.GetCanGoBackAttribute();
				canGoForward = WebNav.GetCanGoForwardAttribute();
			}
			
			if (CanGoBack != canGoBack)
			{
				CanGoBack = canGoBack;
				OnCanGoBackChanged(EventArgs.Empty);
			}
			
			if (CanGoForward != canGoForward)
			{
				CanGoForward = canGoForward;
				OnCanGoForwardChanged(EventArgs.Empty);
			}
		}
		
		/// <summary>
		/// Navigates to the previous page in the history, if one is available.
		/// </summary>
		/// <returns></returns>
		public bool GoBack()
		{
			if (!CanGoBack)
				return false;

			BeginInvoke(new Action(() =>
			{
				try
				{
					WebNav.GoBack();
				}
				catch (COMException ex)
				{
					this.OnNavigationError(new GeckoNavigationErrorEventArgs(this.Url.AbsoluteUri, Window, ex.ErrorCode)); 
				}
			}));
			return true;
		}
		
		/// <summary>
		/// Navigates to the next page in the history, if one is available.
		/// </summary>
		/// <returns></returns>
		public bool GoForward()
		{
			if (!CanGoForward)
				return false;

			BeginInvoke(new Action(() =>
			{
				try
				{
					WebNav.GoForward();
				}
				catch (COMException ex)
				{
					this.OnNavigationError(new GeckoNavigationErrorEventArgs(this.Url.AbsoluteUri, Window, ex.ErrorCode));
				}
			}));
			return true;
		}

        /// <summary>
        /// Cancels any pending navigation and also stops any sound or animation.
        /// </summary>
        public void Stop()
		{
			// We want Stop() to return immediately and to fire events asynchronously. However,
			// WebNav.Stop() may fire 'NavigationError' event synchronously, so we call it asynchronously.
			BeginInvoke(new Action(() =>
			{
				if (WebNav == null)
					return;
				try
				{
					WebNav.Stop((int)nsIWebNavigationConsts.STOP_ALL);
				}
				catch (COMException ex)
				{
					if (ex.ErrorCode == GeckoError.NS_ERROR_UNEXPECTED)
						return;
					throw;
				}
			}));
		}

		/// <summary>
		/// Refresh the current page.
		/// </summary>
		/// <returns></returns>
		public override void Refresh()
		{
		    Refresh(GeckoLoadFlags.None);
		}
		
		/// <summary>
		/// Refresh the current page using the specified flags.
		/// </summary>
		/// <param name="flags"></param>
		/// <returns></returns>
		public bool Refresh(GeckoLoadFlags flags)
		{
			// We want Reload() to return immediately and to fire events asynchronously.
			BeginInvoke(new Action(() =>
			{
				try
				{
                    if (WebNav != null)
					    WebNav.Reload((uint)flags);
				}
				catch (COMException e)
				{
					OnNavigationError(new GeckoNavigationErrorEventArgs(Url.ToString(), Window, e.ErrorCode));
				}
			}));
			
			return true;
		}

        /// <summary>
        /// Executes a JavaScript statement and returns any results
        /// </summary>
        public string ExecuteJavaScript(string javascript)
        {
            GeckoElement resultElement = ExecuteJavaScriptInternal(javascript);
            if (resultElement != null)
            {
                return resultElement.TextContent;
                    
            }
            else
            {
                return string.Empty;
            }
        }

        private GeckoElement ExecuteJavaScriptInternal(string javascript)
        {
            if (this.Document == null)
                return null;

            Guid guid = Guid.NewGuid();
            string resultID = "browseemall_webdriver_gecko_container" + guid.ToString();

            GeckoElement element = this.Document.GetElementById("browseemall_gecko_script_element");
            GeckoScriptElement scriptElement;

            if (element == null)
            {
                scriptElement = (GeckoScriptElement)this.Document.CreateElement("script");
                scriptElement.Type = "text/javascript";
                this.Document.Body.AppendChild(scriptElement);
            }
            else
            {
                scriptElement = (GeckoScriptElement)element;
            }

            string innerFunction = "function innerFunction() { " + javascript + "}";

            scriptElement.Text = "function execute_browseemall_gecko_script() { var element = document.getElementById('" + resultID + "'); if (!element) { element = document.createElement('input');element.type='hidden'; element.id = '" + resultID + "'; document.body.appendChild(element); } else { element.textContent = ''; } element.textContent = innerFunction();};" + innerFunction + " execute_browseemall_gecko_script();";

            GeckoElement resultElement = this.Document.GetElementById(resultID);
            return resultElement;
        }

        /// <summary>
        /// Takes a screenshot of the currently loaded web page (full page screenshot)
        /// </summary>
        public byte[] TakePNGScreenshot()
        {
            return TakePNGScreenshot(this.Document.DocumentElement.ScrollHeight);
        }

        /// <summary>
        /// Takes a screenshot of the currently loaded web page with the specified height
        /// </summary>
        public byte[] TakePNGScreenshot(int height)
        {
            if (this.Document == null)
                return new byte[0];

            ImageCreator creator = new ImageCreator(this);

            byte[] screenshot = creator.CanvasGetPngImage((uint)0, (uint)0, (uint)this.Width, (uint)height);
            return screenshot;
        }

        nsIClipboardCommands ClipboardCommands
		{
			get { return _ClipboardCommands ?? ( _ClipboardCommands = Xpcom.QueryInterface<nsIClipboardCommands>( WebBrowser ) ); }
		}

		nsIClipboardCommands _ClipboardCommands;
		
		delegate bool CanPerformMethod();
		
		bool CanPerform(CanPerformMethod method)
		{
			// in xulrunner (tested on version 5.0)
			// nsIController.IsCommandEnabled("cmd_copyImageContents") can return E_FAIL when clicking on certain objects.
			// this seems to me like a xulrunner bug.

			try
			{
				return method();
			}
			catch (COMException e)
			{
				if ((e.ErrorCode & 0xFFFFFFFF) != 0x80004005)
					throw e;

				return false;
			}
		}
		
		/// <summary>
		/// Gets whether the image contents of the selection may be copied to the clipboard as an image.
		/// </summary>
		[Browsable(false)]
		public bool CanCopyImageContents
		{
			get { return CanPerform(ClipboardCommands.CanCopyImageContents); }
		}
		
		/// <summary>
		/// Copies the image contents of the selection to the clipboard as an image.
		/// </summary>
		/// <returns></returns>
		public bool CopyImageContents()
		{
			if (CanCopyImageContents)
			{
				ClipboardCommands.CopyImageContents();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Returns true if the <see cref="CopyImageLocation"/> command is enabled.
		/// </summary>
		[Browsable(false)]
		public bool CanCopyImageLocation
		{
			get { return CanPerform(ClipboardCommands.CanCopyImageLocation); }
		}
		
		/// <summary>
		/// Copies the location of the currently selected image to the clipboard.
		/// </summary>
		/// <returns></returns>
		public bool CopyImageLocation()
		{
			if (CanCopyImageLocation)
			{
				try
				{
					ClipboardCommands.CopyImageLocation();
				}
				catch (COMException comException)
				{
					if ((comException.ErrorCode & 0xFFFFFFFF) != 0x80004005)
						throw comException;
				}

				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Returns true if the <see cref="CopyLinkLocation"/> command is enabled.
		/// </summary>
		[Browsable(false)]
		public bool CanCopyLinkLocation
		{
			get { return CanPerform(ClipboardCommands.CanCopyLinkLocation); }
		}
		
		/// <summary>
		/// Copies the location of the currently selected link to the clipboard.
		/// </summary>
		/// <returns></returns>
		public bool CopyLinkLocation()
		{
			if (CanCopyLinkLocation)
			{
				ClipboardCommands.CopyLinkLocation();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Returns true if the <see cref="CopySelection"/> command is enabled.
		/// </summary>
		[Browsable(false)]
		public bool CanCopySelection
		{
			get { return CanPerform(ClipboardCommands.CanCopySelection); }
		}
		
		/// <summary>
		/// Copies the selection to the clipboard.
		/// </summary>
		/// <returns></returns>
		public bool CopySelection()
		{
			if (CanCopySelection)
			{
				ClipboardCommands.CopySelection();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Returns true if the <see cref="CutSelection"/> command is enabled.
		/// </summary>
		[Browsable(false)]
		public bool CanCutSelection
		{
			get { return CanPerform(ClipboardCommands.CanCutSelection); }
		}
		
		/// <summary>
		/// Cuts the selection to the clipboard.
		/// </summary>
		/// <returns></returns>
		public bool CutSelection()
		{
			if (CanCutSelection)
			{
				ClipboardCommands.CutSelection();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Returns true if the <see cref="Paste"/> command is enabled.
		/// </summary>
		[Browsable(false)]
		public bool CanPaste
		{
			get { return CanPerform(ClipboardCommands.CanPaste); }
		}
		
		/// <summary>
		/// Pastes the contents of the clipboard at the current selection.
		/// </summary>
		/// <returns></returns>
		public bool Paste()
		{
			if (CanPaste)
			{
				ClipboardCommands.Paste();
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Selects the entire document.
		/// </summary>
		/// <returns></returns>
		public void SelectAll()
		{
			ClipboardCommands.SelectAll();
		}
		
		/// <summary>
		/// Empties the current selection.
		/// </summary>
		/// <returns></returns>
		public void SelectNone()
		{
			ClipboardCommands.SelectNone();
		}
				
		/// <summary>
		/// Gets the <see cref="Url"/> currently displayed in the web browser.
		/// Use the <see cref="Navigate(string)"/> method to change the URL.
		/// </summary>
		[BrowsableAttribute(false)]
		public Uri Url
		{
			get
			{
				if (WebNav == null)
					return null;

				nsIURI locationComObject = WebNav.GetCurrentURIAttribute();
				var uri=locationComObject.ToUri();
				Xpcom.FreeComObject(ref locationComObject);
				return uri ?? new Uri( "about:blank" );
			}
		}

        /// <summary>
        /// Holds the URL of the last successful navigation
        /// </summary>
        public string LastNavigatedUrl { get; set; }
		
		/// <summary>
		/// Gets the <see cref="Url"/> of the current page's referrer.
		/// </summary>
		[BrowsableAttribute(false)]
		public Uri ReferrerUrl
		{
			get
			{
				if (WebNav == null)
					return null;
			
				nsIURI location =  WebNav.GetReferringURIAttribute();
				var uri = location.ToUri();
				Xpcom.FreeComObject(ref location);
				return uri ?? new Uri("about:blank");				
			}
		}

		/// <summary>
		/// Gets the <see cref="GeckoWindow"/> object for this browser.
		/// </summary>
		[Browsable(false)]
		public GeckoWindow Window
		{
			get
			{
				if (WebBrowser == null)
					return null;

				if (_Window != null)
				{
					var window = WebBrowser.GetContentDOMWindowAttribute();
					if (_Window.DomWindow == window)
						return _Window;
					_Window.Dispose();
				}
				_Window = WebBrowser.GetContentDOMWindowAttribute().Wrap( x=>new GeckoWindow( x ) );
				return _Window;
			}
		}

        /// <summary>
        /// Gets the <see cref="GeckoDomDocument"/> for the page currently loaded in the browser.
        /// </summary>
        [Browsable(false)]
        public GeckoDomDocument DomDocument
        {
            get
            {
                if (WebBrowser == null)
                    return null;
                var domDocument = WebNav.GetDocumentAttribute();

                if (_Document != null)
                {
                    if (_Document.NativeDomDocument == domDocument)
                        return _Document;
                    // In some situations when ajax is used dom document wrapper is 1 per page,
                    // therefore we have to create a new one.
                    _Document.Dispose();
                }
                _Document = GeckoDomDocument.CreateDomDocumentWraper(domDocument);
                return _Document;
            }
        }

        /// <summary>
        /// Gets the <see cref="GeckoDocument"/> for the page currently loaded in the browser.
        /// </summary>
        [Browsable(false)]
        public GeckoDocument Document
        {
            get { return DomDocument as GeckoDocument; }
        }

        /// <summary>
        /// Moves the input focus to the browser
        /// </summary>
        public void SetInputFocus()
		{
		}
		
        /// <summary>
        /// Removes the input focus from the browser
        /// </summary>
		public void RemoveInputFocus()
		{
		}
		
		/// <summary>
		/// Gets whether the browser is busy loading a page.
		/// </summary>
		[Browsable(false)]
		public bool IsBusy { get; private set; }

        /// <summary>
        /// Get access to the markup viewer
        /// </summary>
        /// <returns>GeckoMarkupDocumentViewer</returns>
		public GeckoMarkupDocumentViewer GetMarkupDocumentViewer()
		{
			if (WebNav == null)
				return null;

			nsIDocShell shell = Xpcom.QueryInterface<nsIDocShell>(WebNav);
			nsIContentViewer contentViewer = shell.GetContentViewerAttribute();
			Marshal.ReleaseComObject(shell);
			
			return new GeckoMarkupDocumentViewer((nsIMarkupDocumentViewer)contentViewer);
		}
		
		#region nsIWebBrowserChrome Members

		void nsIWebBrowserChrome.SetStatus(uint statusType, string status)
		{
			this.StatusText = status;
            OnStatusTextChanged(EventArgs.Empty);
        }

		nsIWebBrowser nsIWebBrowserChrome.GetWebBrowserAttribute()
		{
			return this.WebBrowser;
		}

		void nsIWebBrowserChrome.SetWebBrowserAttribute(nsIWebBrowser webBrowser)
		{
			this.WebBrowser = webBrowser;
		}

		System.UInt32 nsIWebBrowserChrome.GetChromeFlagsAttribute()
		{
			return this.ChromeFlags;
		}

		void nsIWebBrowserChrome.SetChromeFlagsAttribute(uint flags)
		{
			this.ChromeFlags = (uint)flags;
		}

	    public nsIXULWindow CreateNewWindow(int aChromeFlags, nsITabParent aOpeningTab)
	    {
	        throw new NotImplementedException();
	    }

	    void nsIWebBrowserChrome.DestroyBrowserWindow()
		{
			//throw new NotImplementedException();
			OnWindowClosed(EventArgs.Empty);			
		}

		void nsIWebBrowserChrome.SizeBrowserTo(int cx, int cy)
		{
			OnWindowSetBounds(new GeckoWindowSetBoundsEventArgs(new Rectangle(0, 0, cx, cy), BoundsSpecified.Size));
		}

		void nsIWebBrowserChrome.ShowAsModal()
		{
			//throw new NotImplementedException();
			Debug.WriteLine("ShowAsModal");
			
			_IsWindowModal = true;
		}
		bool _IsWindowModal;
		
		bool nsIWebBrowserChrome.IsWindowModal()
		{
			//throw new NotImplementedException();
			Debug.WriteLine("IsWindowModal");
			
			return _IsWindowModal;
		}

		void nsIWebBrowserChrome.ExitModalEventLoop(int status)
		{
			//throw new NotImplementedException();
			Debug.WriteLine("ExitModalEventLoop");
			
			_IsWindowModal = false;
		}

		#endregion

		#region nsIContextMenuListener2 Members

		void nsIContextMenuListener2.OnShowContextMenu(uint aContextFlags, nsIContextMenuInfo info)
		{
			// if we don't have a target node, we can't do anything by default.  this happens in XUL forms (i.e. about:config)
			if (info.GetTargetNodeAttribute() == null)
				return;
			
			ContextMenu menu = new ContextMenu();
			
			// no default items are added when the context menu is disabled
			if (!this.NoDefaultContextMenu)
			{
				List<MenuItem> optionals = new List<MenuItem>();
				
				optionals.Add(new MenuItem("Back", delegate { GoBack(); }));
				optionals.Add(new MenuItem("Forward", delegate { GoForward(); }));
					
				optionals[0].Enabled = this.CanGoBack;
				optionals[1].Enabled = this.CanGoForward;
				
				optionals.Add(new MenuItem("-"));
				
				if (this.CanCopyImageContents)
					optionals.Add(new MenuItem("Copy Image Contents", delegate { CopyImageContents(); }));
				
				if (this.CanCopyImageLocation)
					optionals.Add(new MenuItem("Copy Image Location", delegate { CopyImageLocation(); }));
				
				if (this.CanCopyLinkLocation)
					optionals.Add(new MenuItem("Copy Link Location", delegate { CopyLinkLocation(); }));
				
				if (this.CanCopySelection)
					optionals.Add(new MenuItem("Copy Selection", delegate { CopySelection(); }));
				
				MenuItem mnuSelectAll = new MenuItem("Select All");
				mnuSelectAll.Click += delegate { SelectAll(); };

				GeckoDomDocument doc = GeckoDomDocument.CreateDomDocumentWraper(info.GetTargetNodeAttribute().GetOwnerDocumentAttribute());

				string viewSourceUrl = (doc == null) ? null : Convert.ToString(doc.Uri);
				
				MenuItem mnuViewSource = new MenuItem("View Source");
				mnuViewSource.Enabled = !string.IsNullOrEmpty(viewSourceUrl);
				mnuViewSource.Click += delegate { ViewSource(viewSourceUrl); };

				MenuItem mnuOpenInSystemBrowser = new MenuItem("View In System Browser");//nice for debugging with firefox/firebug
				mnuOpenInSystemBrowser.Enabled = !string.IsNullOrEmpty(viewSourceUrl);
				mnuOpenInSystemBrowser.Click += delegate { ViewInSystemBrowser(viewSourceUrl); };


				menu.MenuItems.AddRange(optionals.ToArray());
				menu.MenuItems.Add(mnuSelectAll);
				menu.MenuItems.Add("-");
				menu.MenuItems.Add(mnuViewSource);
				menu.MenuItems.Add(mnuOpenInSystemBrowser);
			}

			// get image urls
			Uri backgroundImageSrc = null, imageSrc = null;
			nsIURI src;
			try
			{
				src = info.GetBackgroundImageSrcAttribute();
				backgroundImageSrc = src.ToUri();
				Marshal.ReleaseComObject( src );
			}
			catch (COMException comException)
			{
				if ((comException.ErrorCode & 0xFFFFFFFF) != 0x80004005)
					throw comException;
			}

			try
			{
				src = info.GetImageSrcAttribute();
				if ( src != null )
				{
					imageSrc = src.ToUri();
					Marshal.ReleaseComObject( src );
				}
			}
			catch (COMException comException)
			{
				if ((comException.ErrorCode & 0xFFFFFFFF) != 0x80004005)
					throw comException;
			}
			
			// get associated link.  note that this needs to be done manually because GetAssociatedLink returns a non-zero
			// result when no associated link is available, so an exception would be thrown by nsString.Get()
			string associatedLink = null;
			try
			{
				using (nsAString str = new nsAString())
				{
					info.GetAssociatedLinkAttribute(str);
					associatedLink = str.ToString();
				}
			}
			catch (COMException) { }			
			
			GeckoContextMenuEventArgs e = new GeckoContextMenuEventArgs(
				PointToClient(MousePosition), menu, associatedLink, backgroundImageSrc, imageSrc,
				GeckoNode.Create(Xpcom.QueryInterface<nsIDOMNode>(info.GetTargetNodeAttribute()))
				);
			
			OnShowContextMenu(e);
			
			if (e.ContextMenu != null && e.ContextMenu.MenuItems.Count > 0)
			{
				e.ContextMenu.Show(this, e.Location);
			}
		}

		private void ViewInSystemBrowser(string url)
		{
			Process.Start(url);
		}
		
		/// <summary>
		/// Opens a new window which contains the source code for the current page.
		/// </summary>
		public void ViewSource()
		{
			ViewSource(Url.ToString());
		}
		
		/// <summary>
		/// Opens a new window which contains the source code for the specified page.
		/// </summary>
		/// <param name="url"></param>
		public void ViewSource(string url)
		{
			Form form = new Form();
			form.Text = "View Source";
			GeckoWebBrowser browser = new GeckoWebBrowser();
			browser.Dock = DockStyle.Fill;
			form.Controls.Add(browser);
			form.Load += delegate { browser.Navigate("view-source:" + url); };
			try
			{
                //TODO: when gecko is used in WPF application there are no forms. We should rewrite this code
				var outerForm = FindForm();
				if (outerForm != null)
				{
					form.Icon = outerForm.Icon;
				}
			}
			catch ( Exception )
			{

			}
			
			form.ClientSize = this.ClientSize;
			form.StartPosition = FormStartPosition.CenterParent;
			form.Show();			
		}
			
		#endregion

		#region nsIInterfaceRequestor Members

		IntPtr nsIInterfaceRequestor.GetInterface(ref Guid uuid)
		{
			object obj = this;
			
			// note: when a new window is created, gecko calls GetInterface on the webbrowser to get a DOMWindow in order
			// to set the starting url
			if (this.WebBrowser != null)
			{
				if (uuid == typeof(nsIDOMWindow).GUID)
				{
					obj = this.WebBrowser.GetContentDOMWindowAttribute();
				}
				/*else if (uuid == typeof(nsIDOMDocument).GUID)
				{
					obj = this.WebBrowser.GetContentDOMWindowAttribute().GetDocumentAttribute();
				}*/
			}
			
			IntPtr ppv, pUnk = Marshal.GetIUnknownForObject(obj);
			
			Marshal.QueryInterface(pUnk, ref uuid, out ppv);
			
			Marshal.Release(pUnk);

			return ppv;
		}

		#endregion

		#region nsIEmbeddingSiteWindow Members

		void nsIEmbeddingSiteWindow.SetDimensions(uint flags, int x, int y, int cx, int cy)
		{
			const int DIM_FLAGS_POSITION = 1;
			const int DIM_FLAGS_SIZE_INNER = 2;
			const int DIM_FLAGS_SIZE_OUTER = 4;
			
			BoundsSpecified specified = 0;
			if ((flags & DIM_FLAGS_POSITION) != 0)
			{
				specified |= BoundsSpecified.Location;
			}
			if ((flags & DIM_FLAGS_SIZE_INNER) != 0 || (flags & DIM_FLAGS_SIZE_OUTER) != 0)
			{
				specified |= BoundsSpecified.Size;
			}
			
			OnWindowSetBounds(new GeckoWindowSetBoundsEventArgs(new Rectangle(x, y, cx, cy), specified));			
		}

		unsafe void nsIEmbeddingSiteWindow.GetDimensions(uint flags, int* x, int* y, int* cx, int* cy)
		{
			int localX = ( x != ( void* ) 0 ) ? *x : 0;
			int localY = ( y != ( void* ) 0 ) ? *y : 0;
			int localCX = 0;
			int localCY = 0;
			if ( !IsDisposed )
			{
				if ( ( flags & nsIEmbeddingSiteWindowConstants.DIM_FLAGS_POSITION ) != 0 )
				{
					Point pt = PointToScreen( Point.Empty );
					localX = pt.X;
					localY = pt.Y;
				}
				localCX = ClientSize.Width;
				localCY = ClientSize.Height;

				if ( ( this.ChromeFlags & ( int ) GeckoWindowFlags.OpenAsChrome ) != 0 )
				{
					BaseWindow.GetSize( ref localCX, ref localCY );
				}

				if ( ( flags & nsIEmbeddingSiteWindowConstants.DIM_FLAGS_SIZE_INNER ) == 0 )
				{
					Control topLevel = TopLevelControl;
					if ( topLevel != null )
					{
						Size nonClient = new Size( topLevel.Width - ClientSize.Width, topLevel.Height - ClientSize.Height );
						localCX += nonClient.Width;
						localCY += nonClient.Height;
					}
				}
			}
			if (x != (void*)0) *x = localX;
			if (y != (void*)0) *y = localY;
			if (cx != (void*)0) *cx = localCX;
			if (cy != (void*)0) *cy = localCY;
		}

		void nsIEmbeddingSiteWindow.SetFocus()
		{
			Focus();
			if (BaseWindow != null)
			{
				BaseWindow.SetFocus();
			}			
		}

		bool nsIEmbeddingSiteWindow.GetVisibilityAttribute()
		{
			return Visible;
		}

		void nsIEmbeddingSiteWindow.SetVisibilityAttribute(bool aVisibility)
		{
			//if (aVisibility)
			//{
			//      Form form = FindForm();
			//      if (form != null)
			//      {
			//            form.Visible = aVisibility;
			//      }
			//}
			
			Visible = aVisibility;
		}

		string nsIEmbeddingSiteWindow.GetTitleAttribute()
		{
			return DocumentTitle;
		}

		void nsIEmbeddingSiteWindow.SetTitleAttribute(string aTitle)
		{
			DocumentTitle = aTitle;
            OnDocumentTitleChanged(EventArgs.Empty);
        }

		void nsIEmbeddingSiteWindow.Blur()
		{
			// TODO: implement.
		}

		#endregion		
		
		#region nsIWebProgressListener Members

		void nsIWebProgressListener.OnStateChange(nsIWebProgress aWebProgress, nsIRequest aRequest, uint aStateFlags, int aStatus)
        {
			const int NS_BINDING_ABORTED = unchecked((int)0x804B0002);
			
			// The request parametere may be null
			if (aRequest == null)
				return;

			// Ignore ViewSource requests, they don't provide the URL
			// see: http://mxr.mozilla.org/mozilla-central/source/netwerk/protocol/viewsource/nsViewSourceChannel.cpp#114
			{
				var viewSource = Xpcom.QueryInterface<nsIViewSourceChannel>( aRequest );
				if ( viewSource != null )
				{
					Marshal.ReleaseComObject( viewSource );
					return;
				}
			}
	
			using (var request = Request.CreateRequest(aRequest))
			{
				#region request parameters
				Uri destUri = null;
				Uri.TryCreate(request.Name, UriKind.Absolute, out destUri);
				var domWindow = aWebProgress.GetDOMWindowAttribute().Wrap(x => new GeckoWindow(x));

				/* This flag indicates that the state transition is for a request, which includes but is not limited to document requests.
				 * Other types of requests, such as requests for inline content (for example images and stylesheets) are considered normal requests.
				 */
				bool stateIsRequest = ((aStateFlags & nsIWebProgressListenerConstants.STATE_IS_REQUEST) != 0);

				/* This flag indicates that the state transition is for a document request. This flag is set in addition to STATE_IS_REQUEST.
				 * A document request supports the nsIChannel interface and its loadFlags attribute includes the nsIChannel ::LOAD_DOCUMENT_URI flag.
				 * A document request does not complete until all requests associated with the loading of its corresponding document have completed.
				 * This includes other document requests (for example corresponding to HTML <iframe> elements).
				 * The document corresponding to a document request is available via the DOMWindow attribute of onStateChange()'s aWebProgress parameter.
				 */
				bool stateIsDocument = ((aStateFlags & nsIWebProgressListenerConstants.STATE_IS_DOCUMENT) != 0);

				/* This flag indicates that the state transition corresponds to the start or stop of activity in the indicated nsIWebProgress instance.
				 * This flag is accompanied by either STATE_START or STATE_STOP, and it may be combined with other State Type Flags.
				 * 
				 * Unlike STATE_IS_WINDOW, this flag is only set when activity within the nsIWebProgress instance being observed starts or stops.
				 * If activity only occurs in a child nsIWebProgress instance, then this flag will be set to indicate the start and stop of that activity.
				 * For example, in the case of navigation within a single frame of a HTML frameset, a nsIWebProgressListener instance attached to the
				 * nsIWebProgress of the frameset window will receive onStateChange() calls with the STATE_IS_NETWORK flag set to indicate the start and
				 * stop of said navigation. In other words, an observer of an outer window can determine when activity, that may be constrained to a
				 * child window or set of child windows, starts and stops.
				 */
				bool stateIsNetwork = ((aStateFlags & nsIWebProgressListenerConstants.STATE_IS_NETWORK) != 0);

				/* This flag indicates that the state transition corresponds to the start or stop of activity in the indicated nsIWebProgress instance.
				 * This flag is accompanied by either STATE_START or STATE_STOP, and it may be combined with other State Type Flags.
				 * This flag is similar to STATE_IS_DOCUMENT. However, when a document request completes, two onStateChange() calls with STATE_STOP are generated.
				 * The document request is passed as aRequest to both calls. The first has STATE_IS_REQUEST and STATE_IS_DOCUMENT set, and the second has
				 * the STATE_IS_WINDOW flag set (and possibly the STATE_IS_NETWORK flag set as well -- see above for a description of when the STATE_IS_NETWORK
				 * flag may be set). This second STATE_STOP event may be useful as a way to partition the work that occurs when a document request completes.
				 */
				bool stateIsWindow = ((aStateFlags & nsIWebProgressListenerConstants.STATE_IS_WINDOW) != 0);
				#endregion request parameters

				#region STATE_START
				/* This flag indicates the start of a request.
				 * This flag is set when a request is initiated.
				 * The request is complete when onStateChange() is called for the same request with the STATE_STOP flag set.
				 */
				if ((aStateFlags & nsIWebProgressListenerConstants.STATE_START) != 0)
				{
					if (stateIsNetwork && aWebProgress.GetIsTopLevelAttribute())
					{
						IsBusy = true;

						GeckoNavigatingEventArgs ea = new GeckoNavigatingEventArgs(destUri, domWindow);
						OnNavigating(ea);

						if (ea.Cancel)
						{
							aRequest.Cancel(NS_BINDING_ABORTED);

							// clear busy state
							IsBusy = false;

							// clear progress bar
							OnProgressChanged(new GeckoProgressEventArgs(100, 100));

							// clear status bar
							StatusText = "";
                            OnStatusTextChanged(EventArgs.Empty);
                        }
					}
					else if (stateIsDocument)
					{
						GeckoNavigatingEventArgs ea = new GeckoNavigatingEventArgs(destUri, domWindow);
						OnFrameNavigating(ea);

						if (ea.Cancel)
						{
							if (!Xpcom.IsLinux)
								aRequest.Cancel(NS_BINDING_ABORTED);
						}
					}
				}
				#endregion STATE_START

				#region STATE_REDIRECTING
				/* This flag indicates that a request is being redirected.
				 * The request passed to onStateChange() is the request that is being redirected.
				 * When a redirect occurs, a new request is generated automatically to process the new request.
				 * Expect a corresponding STATE_START event for the new request, and a STATE_STOP for the redirected request.
				 */
				else if ((aStateFlags & nsIWebProgressListenerConstants.STATE_REDIRECTING) != 0)
				{

					// make sure we're loading the top-level window
					GeckoRedirectingEventArgs ea = new GeckoRedirectingEventArgs(destUri, domWindow);
					OnRedirecting(ea);

					if (ea.Cancel)
					{
						aRequest.Cancel(NS_BINDING_ABORTED);
					}
				}
				#endregion STATE_REDIRECTING

				#region STATE_TRANSFERRING
				/* This flag indicates that data for a request is being transferred to an end consumer.
				 * This flag indicates that the request has been targeted, and that the user may start seeing content corresponding to the request.
				 */
				else if ((aStateFlags & nsIWebProgressListenerConstants.STATE_TRANSFERRING) != 0)
				{
				}
				#endregion STATE_TRANSFERRING

				#region STATE_STOP
				/* This flag indicates the completion of a request.
				 * The aStatus parameter to onStateChange() indicates the final status of the request.
				 */
				else if ((aStateFlags & nsIWebProgressListenerConstants.STATE_STOP) != 0)
				{
					/* aStatus
					 * Error status code associated with the state change.
					 * This parameter should be ignored unless aStateFlags includes the STATE_STOP bit.
					 * The status code indicates success or failure of the request associated with the state change.
					 * 
					 * Note: aStatus may be a success code even for server generated errors, such as the HTTP 404 File Not Found error.
					 * In such cases, the request itself should be queried for extended error information (for example for HTTP requests see nsIHttpChannel).
					 */

					if (stateIsNetwork)
					{
						// clear busy state
						IsBusy = false;
						if (aStatus == 0)
						{
							// navigating to a unrenderable file (.zip, .exe, etc.) causes the request pending;
							// also an OnStateChange call with aStatus:804B0004(NS_BINDING_RETARGETED) has been generated previously.
							if (!request.IsPending)
							{
								// kill any cached document and raise DocumentCompleted event
                                if (domWindow != null /*&& domWindow.IsTopWindow()*/)
								    OnDocumentCompleted(new GeckoDocumentCompletedEventArgs(destUri, domWindow));

								// clear progress bar
								OnProgressChanged(new GeckoProgressEventArgs(100, 100));
							}
						}
						else
						{
							OnNavigationError(new GeckoNavigationErrorEventArgs(request.Name, domWindow, aStatus));
						}
						
                        // clear status bar
						StatusText = "";
                        OnStatusTextChanged(EventArgs.Empty);
                    }

					if (stateIsRequest)
					{
                        if ((aStatus & 0xff0000) == ((GeckoError.NS_ERROR_MODULE_SECURITY + GeckoError.NS_ERROR_MODULE_BASE_OFFSET) << 16))
                        {
                            nsISSLStatus sslStatus = null;
                            nsIChannel aChannel = null;
                            nsISupports aSecInfo = null;
                            nsISSLStatusProvider aSslStatusProv = null;
                            try
                            {
                                aChannel = Xpcom.QueryInterface<nsIChannel>(aRequest);
                                if (aChannel != null)
                                {
                                    aSecInfo = aChannel.GetSecurityInfoAttribute();
                                    if (aSecInfo != null)
                                    {
                                        aSslStatusProv = Xpcom.QueryInterface<nsISSLStatusProvider>(aSecInfo);
                                        if (aSslStatusProv != null)
                                        {
                                            sslStatus = aSslStatusProv.GetSSLStatusAttribute();
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                Xpcom.FreeComObject(ref aChannel);
                                Xpcom.FreeComObject(ref aSecInfo);
                                Xpcom.FreeComObject(ref aSslStatusProv);
                            }
                           
                            var ea = new GeckoNSSErrorEventArgs(destUri, aStatus, sslStatus);
                            OnNSSError(ea);
							if (ea.Handled)
							{
								aRequest.Cancel(GeckoError.NS_BINDING_ABORTED);
							}
						}

						if (aStatus == GeckoError.NS_BINDING_RETARGETED)
						{
							GeckoFileDownloadEventArgs ea = new GeckoFileDownloadEventArgs(destUri, domWindow, request);
							OnFileDownload(ea);
						}
					}
				}
				#endregion STATE_STOP
				if (domWindow!=null)
				{
					domWindow.Dispose();
				}
			}
		}

		void nsIWebProgressListener.OnProgressChange(nsIWebProgress aWebProgress, nsIRequest aRequest, int aCurSelfProgress, int aMaxSelfProgress, int aCurTotalProgress, int aMaxTotalProgress)
		{
			// Note: If any progress value is unknown, then its value is replaced with -1.
			using (var request = Request.CreateRequest(aRequest))
			{
				if ((aCurSelfProgress != -1) && (aMaxSelfProgress != -1))
					OnRequestProgressChanged(new GeckoRequestProgressEventArgs(aCurTotalProgress, aMaxTotalProgress, aRequest));

				if ((aCurTotalProgress != -1) && (aMaxTotalProgress != -1))
					OnProgressChanged(new GeckoProgressEventArgs(aCurTotalProgress, aMaxTotalProgress));
			}
		}

		void nsIWebProgressListener.OnLocationChange(nsIWebProgress aWebProgress, nsIRequest aRequest, nsIURI aLocation, uint flags)
		{
			if (IsDisposed) return;

			Uri uri = new Uri(nsString.Get(aLocation.GetSpecAttribute));
			using (var domWindow = aWebProgress.GetDOMWindowAttribute().Wrap(x=>new GeckoWindow(x)))
			{

				bool sameDocument = ( flags & nsIWebProgressListenerConstants.LOCATION_CHANGE_SAME_DOCUMENT ) != 0;
				bool errorPage = ( flags & nsIWebProgressListenerConstants.LOCATION_CHANGE_ERROR_PAGE ) != 0;
				var ea = new GeckoNavigatedEventArgs( uri, aRequest, domWindow, sameDocument, errorPage );

                if (!uri.AbsoluteUri.Equals("about:blank"))
                    LastNavigatedUrl = uri.AbsoluteUri;

                if (!uri.AbsoluteUri.Equals("about:blank") && !uri.AbsoluteUri.Contains("/target/target-script-min.js"))
                    BaseWindow.SetVisibilityAttribute(true);

                if (!uri.AbsoluteUri.Contains("/target/target-script-min.js"))
                    OnNavigated( ea );
			}
			UpdateCommandStatus();
		}

		void nsIWebProgressListener.OnStatusChange(nsIWebProgress aWebProgress, nsIRequest aRequest, int aStatus, string aMessage)
		{
			if (aWebProgress.GetIsLoadingDocumentAttribute())
			{
				StatusText = aMessage;
				UpdateCommandStatus();
                OnStatusTextChanged(EventArgs.Empty);
            }
		}

		void nsIWebProgressListener.OnSecurityChange(nsIWebProgress aWebProgress, nsIRequest aRequest, uint aState)
		{
			SecurityState = ( GeckoSecurityState ) aState;
		}
		
		/// <summary>
		/// Gets a value which indicates whether the current page is secure.
		/// </summary>
		[Browsable(false)]
		public GeckoSecurityState SecurityState
		{
			get { return _SecurityState; }
			private set
			{
				if (_SecurityState == value) return;
				_SecurityState = value;
				OnSecurityStateChanged(EventArgs.Empty);
			}
		}
		GeckoSecurityState _SecurityState;
	
		
		#region public event EventHandler SecurityStateChanged
		/// <summary>
		/// Occurs when the value of the <see cref="SecurityState"/> property is changed.
		/// </summary>
		[Category("Property Changed"), Description("Occurs when the value of the SecurityState property is changed.")]
		public event EventHandler SecurityStateChanged
		{
			add { this.Events.AddHandler(SecurityStateChangedEvent, value); }
			remove { this.Events.RemoveHandler(SecurityStateChangedEvent, value); }
		}
		private static object SecurityStateChangedEvent = new object();

		/// <summary>Raises the <see cref="SecurityStateChanged"/> event.</summary>
		/// <param name="e">The data for the event.</param>
		protected virtual void OnSecurityStateChanged(EventArgs e)
		{
			if (((EventHandler)this.Events[SecurityStateChangedEvent]) != null)
				((EventHandler)this.Events[SecurityStateChangedEvent])(this, e);
		}
		#endregion

		#endregion

		#region nsIWebProgressListener2 Members

		#region implemented in nsIWebProgressListener
		void nsIWebProgressListener2.OnStateChange(nsIWebProgress aWebProgress, nsIRequest aRequest, uint aStateFlags, int aStatus)
		{
			throw new NotImplementedException("implemented in nsIWebProgressListener");
		}

		void nsIWebProgressListener2.OnProgressChange(nsIWebProgress aWebProgress, nsIRequest aRequest, int aCurSelfProgress, int aMaxSelfProgress, int aCurTotalProgress, int aMaxTotalProgress)
		{
			throw new NotImplementedException("implemented in nsIWebProgressListener");
		}

		void nsIWebProgressListener2.OnLocationChange(nsIWebProgress aWebProgress, nsIRequest aRequest, nsIURI aLocation, uint aFlags)
		{
			throw new NotImplementedException("implemented in nsIWebProgressListener");
		}

		void nsIWebProgressListener2.OnStatusChange(nsIWebProgress aWebProgress, nsIRequest aRequest, int aStatus, string aMessage)
		{
			throw new NotImplementedException("implemented in nsIWebProgressListener");
		}

		void nsIWebProgressListener2.OnSecurityChange(nsIWebProgress aWebProgress, nsIRequest aRequest, uint aState)
		{
			throw new NotImplementedException("implemented in nsIWebProgressListener");
		}
		#endregion implemented in nsIWebProgressListener

		void nsIWebProgressListener2.OnProgressChange64(nsIWebProgress aWebProgress, nsIRequest aRequest, long aCurSelfProgress, long aMaxSelfProgress, long aCurTotalProgress, long aMaxTotalProgress)
		{
			// Note: If any progress value is unknown, then its value is replaced with -1.

			if ((aCurSelfProgress != -1) && (aMaxSelfProgress != -1))
				OnRequestProgressChanged(new GeckoRequestProgressEventArgs(aCurTotalProgress, aMaxSelfProgress, aRequest));

			if ((aCurTotalProgress != -1) && (aMaxTotalProgress != -1))
				OnProgressChanged(new GeckoProgressEventArgs(aCurTotalProgress, aMaxTotalProgress));
		}

		bool nsIWebProgressListener2.OnRefreshAttempted( nsIWebProgress aWebProgress, nsIURI aRefreshURI, int aMillis, bool aSameURI )
		{
			Uri destUri = new Uri( nsString.Get( aRefreshURI.GetSpecAttribute ) );
			bool cancel = false;
			using (var domWindow = aWebProgress.GetDOMWindowAttribute().Wrap(x=>new GeckoWindow(x)))
			{
				GeckoNavigatingEventArgs ea = new GeckoNavigatingEventArgs( destUri, domWindow );
				cancel = ea.Cancel;
			}
			//OnRefreshAttempt();
			return !cancel;
		}

		#endregion

		#region nsIWindowProvider Members
		//int nsIWindowProvider.provideWindow(nsIDOMWindow aParent, uint aChromeFlags, bool aPositionSpecified, bool aSizeSpecified, nsIURI aURI, nsAString aName, nsAString aFeatures, out bool aWindowIsNew, out nsIDOMWindow ret)
		//{
		//      if (!this.IsDisposed)
		//      {
		//            GeckoCreateWindowEventArgs e = new GeckoCreateWindowEventArgs((GeckoWindowFlags)aChromeFlags);                
		//            this.OnCreateWindow(e);
				
		//            if (e.WebBrowser != null)
		//            {
		//                  aWindowIsNew = true;
		//                  ret = (nsIDOMWindow)e.WebBrowser.Window.DomWindow;
		//                  return 0;
		//            }
		//            else
		//            {
		//                  System.Media.SystemSounds.Beep.Play();
		//            }
		//      }
			
		//      aWindowIsNew = true;
		//      ret = null;
		//      return -1;
		//}
		#endregion

		#region nsISHistoryListener Members
		void nsISHistoryListener.OnHistoryNewEntry(nsIURI aNewURI)
		{
			OnHistoryNewEntry(new GeckoHistoryEventArgs(new Uri(nsString.Get(aNewURI.GetSpecAttribute))));
		}

		bool nsISHistoryListener.OnHistoryGoBack(nsIURI aBackURI)
		{
			GeckoHistoryEventArgs e = new GeckoHistoryEventArgs(new Uri(nsString.Get(aBackURI.GetSpecAttribute)));
			OnHistoryGoBack(e);
			return !e.Cancel;
		}

		bool nsISHistoryListener.OnHistoryGoForward(nsIURI aForwardURI)
		{
			GeckoHistoryEventArgs e = new GeckoHistoryEventArgs(new Uri(nsString.Get(aForwardURI.GetSpecAttribute)));
			OnHistoryGoForward(e);
			return !e.Cancel;
		}

		bool nsISHistoryListener.OnHistoryReload(nsIURI aReloadURI, uint aReloadFlags)
		{
			GeckoHistoryEventArgs e = new GeckoHistoryEventArgs(new Uri(nsString.Get(aReloadURI.GetSpecAttribute)));
			OnHistoryReload(e);
			return !e.Cancel;
		}

		bool nsISHistoryListener.OnHistoryGotoIndex(int aIndex, nsIURI aGotoURI)
		{
			GeckoHistoryGotoIndexEventArgs e = new GeckoHistoryGotoIndexEventArgs(new Uri(nsString.Get(aGotoURI.GetSpecAttribute)), aIndex);
			OnHistoryGotoIndex(e);
			return !e.Cancel;
		}

		bool nsISHistoryListener.OnHistoryPurge(int aNumEntries)
		{
			GeckoHistoryPurgeEventArgs e = new GeckoHistoryPurgeEventArgs(aNumEntries);
			OnHistoryPurge(e);
			return !e.Cancel;
		}

        void nsISHistoryListener.OnHistoryReplaceEntry(int aIndex)
	    {
            OnHistoryReplaceEntry(new GeckoHistoryRepalaceEntryEventArgs(aIndex));
	    }

	    #endregion

		#region nsITooltipListener Members

		void nsITooltipListener.OnShowTooltip(int aXCoords, int aYCoords, string aTipText)
		{
			if (true.Equals(GeckoPreferences.User["browser.chrome.toolbar_tips"]))
			{
				ToolTip = new ToolTipWindow();
				ToolTip.Show(aTipText, this, new Point(aXCoords, aYCoords + 24));
			}
		}
		
		ToolTipWindow ToolTip;

		void nsITooltipListener.OnHideTooltip()
		{
			if (ToolTip != null)
			{
				ToolTip.Hide(this);
				ToolTip.Dispose();
				ToolTip = null;
			}
			
		}		
		#endregion

		public nsIWeakReference GetWeakReference()
		{
			return new ControlWeakReference( this );
		}
	}
	
	public enum GeckoSecurityState
	{
		/// <summary>
		/// This flag indicates that the data corresponding to the request was received over an insecure channel.
		/// </summary>
		Insecure = unchecked((int)nsIWebProgressListenerConstants.STATE_IS_INSECURE),
		/// <summary>
		/// This flag indicates an unknown security state.  This may mean that the request is being loaded as part of
		/// a page in which some content was received over an insecure channel.
		/// </summary>
		Broken = unchecked((int)nsIWebProgressListenerConstants.STATE_IS_BROKEN),
		/// <summary>
		/// This flag indicates that the data corresponding to the request was received over a secure channel.
		/// The degree of security is expressed by GeckoSecurityStrength.
		/// </summary>
		Secure = unchecked((int)nsIWebProgressListenerConstants.STATE_IS_SECURE),
	}

}
