﻿using BrowseEmAll.Gecko.Core;
using System;

namespace BrowseEmAll.Gecko.Winforms
{
    public class WindowCreator : nsIWindowCreator2
    {
        static WindowCreator()
        {
            // call window watcher service
            Core.Services.WindowWatcher.WindowCreator = new WindowCreator();
        }

        public static void Register()
        {
            // calling this method simply invokes the static ctor
        }

        private nsIWebBrowserChrome DoCreateChromeWindow(nsIWebBrowserChrome parent, uint chromeFlags, uint contextFlags, nsIURI uri, ref bool cancel)
        {
            var url = "";
            if (uri != null)
                url = (nsString.Get(uri.GetSpecAttribute)).ToString();
            else
                url = "about:blank";

            // for chrome windows, we can use the AppShellService to create the window using some built-in xulrunner code
            GeckoWindowFlags flags = (GeckoWindowFlags)chromeFlags;
            if ((flags & GeckoWindowFlags.OpenAsChrome) != 0)
            {
                // obtain the services we need
                // nsIAppShellService appShellService = Xpcom.GetService<nsIAppShellService>("@mozilla.org/appshell/appShellService;1");

                // create the child window
                nsIXULWindow xulChild = AppShellService.CreateTopLevelWindow(null, null, chromeFlags, -1, -1);

                // this little gem allows the GeckoWebBrowser to be properly activated when it gains the focus again
                if (parent is GeckoWebBrowser && (flags & GeckoWindowFlags.OpenAsDialog) != 0)
                {
                    EventHandler gotFocus = null;
                    gotFocus = delegate (object sender, EventArgs e)
                    {
                        var geckoWebBrowser = (GeckoWebBrowser)sender;
                        geckoWebBrowser.GotFocus -= gotFocus;

                        if (geckoWebBrowser.WebBrowserFocus != null)
                            geckoWebBrowser.WebBrowserFocus.Activate();
                    };
                    (parent as GeckoWebBrowser).GotFocus += gotFocus;
                }

                // return the chrome
                return Xpcom.QueryInterface<nsIWebBrowserChrome>(xulChild);
            }

            GeckoWebBrowser browser = parent as GeckoWebBrowser;
            if (browser != null)
            {
                var e = new GeckoCreateWindow2EventArgs(flags, url);
                if (uri != null) // called by CreateChromeWindow2()
                    browser.OnCreateWindow2(e);

                browser.OnNewWindow(e);

                if (e.Cancel)
                {
                    cancel = true;
                    return null;
                }

                if (e.WebBrowser != null)
                {
                    // set flags
                    ((nsIWebBrowserChrome)e.WebBrowser).SetChromeFlagsAttribute(chromeFlags);
                    return e.WebBrowser;
                }

                nsIXULWindow xulChild = AppShellService.CreateTopLevelWindow(null, null, chromeFlags, e.InitialWidth, e.InitialHeight);
                return Xpcom.QueryInterface<nsIWebBrowserChrome>(xulChild);
            }
            return null;
        }

        public nsIWebBrowserChrome CreateChromeWindow(nsIWebBrowserChrome parent, uint chromeFlags)
        {
            bool cancel = false;
            return DoCreateChromeWindow(parent, chromeFlags, 0, null, ref cancel);
        }

        public nsIWebBrowserChrome CreateChromeWindow2(nsIWebBrowserChrome parent, uint chromeFlags, uint contextFlags, nsIURI uri, nsITabParent aOpeningTab, ref bool cancel)
        {
            return DoCreateChromeWindow(parent, chromeFlags, contextFlags, uri, ref cancel);
        }
    }


}
