﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoRequestProgressEventArgs
        : EventArgs
    {
        private nsIRequest _request;
        private GeckoResponse _reqWrapper;

        public readonly long CurrentProgress;
        public readonly long MaximumProgress;
        /// <summary>Creates a new instance of a <see cref="GeckoRequestProgressEventArgs"/> object.</summary>
        public GeckoRequestProgressEventArgs(long current, long max, nsIRequest req)
        {
            CurrentProgress = current;
            MaximumProgress = max;
            _request = req;
        }

        public GeckoResponse Request
        {
            get
            {
                return _reqWrapper ?? (_reqWrapper = new GeckoResponse(_request));
            }
        }
    }

}
