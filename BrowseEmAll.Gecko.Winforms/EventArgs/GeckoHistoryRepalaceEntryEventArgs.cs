﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoHistoryRepalaceEntryEventArgs : CancelEventArgs
    {
        public readonly int Index;

        public GeckoHistoryRepalaceEntryEventArgs(int index)
        {
            Index = index;
        }
    }
}
