﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoProgressEventArgs
        : EventArgs
    {
        public readonly long CurrentProgress;
        public readonly long MaximumProgress;
        /// <summary>Creates a new instance of a <see cref="GeckoProgressEventArgs"/> object.</summary>
        public GeckoProgressEventArgs(long current, long max)
        {
            CurrentProgress = current;
            MaximumProgress = max;
        }
    }

}
