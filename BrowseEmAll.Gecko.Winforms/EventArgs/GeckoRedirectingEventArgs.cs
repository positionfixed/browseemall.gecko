﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoRedirectingEventArgs
        : CancelEventArgs
    {
        public readonly Uri Uri;
        public readonly GeckoWindow DomWindow;

        /// <summary>Creates a new instance of a <see cref="GeckoRedirectingEventArgs"/> object.</summary>
        /// <param name="value"></param>
        public GeckoRedirectingEventArgs(Uri value, GeckoWindow domWind)
        {
            Uri = value;
            DomWindow = domWind;
        }
    }

}
