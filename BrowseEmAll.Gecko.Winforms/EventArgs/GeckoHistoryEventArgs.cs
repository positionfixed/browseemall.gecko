﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoHistoryEventArgs
        : CancelEventArgs
    {
        /// <summary>
        /// Gets the URL of the history entry.
        /// </summary>
        public readonly Uri Url;

        /// <summary>Creates a new instance of a <see cref="GeckoHistoryEventArgs"/> object.</summary>
        /// <param name="url"></param>
        public GeckoHistoryEventArgs(Uri url)
        {
            Url = url;
        }
    }

}
