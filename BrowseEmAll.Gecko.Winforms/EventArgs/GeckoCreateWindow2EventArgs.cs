﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoCreateWindow2EventArgs
        : GeckoCreateWindowEventArgs
    {
        /// <summary>Creates a new instance of a <see cref="GeckoCreateWindowEventArgs"/> object.</summary>
        /// <param name="flags"></param>
        /// <param name="uri"></param>
        public GeckoCreateWindow2EventArgs(GeckoWindowFlags flags, String uri)
            : base(flags, uri)
        {
        }
    }
}
