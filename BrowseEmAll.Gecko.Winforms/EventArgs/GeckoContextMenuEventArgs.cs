﻿using BrowseEmAll.Gecko.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrowseEmAll.Gecko.Winforms
{
    public class GeckoContextMenuEventArgs
        : EventArgs
    {
        /// <summary>
        /// Gets the location where the context menu will be displayed.
        /// </summary>
        public readonly Point Location;
        public readonly Uri BackgroundImageSrc;
        public readonly Uri ImageSrc;
        public readonly string AssociatedLink;

        private GeckoNode _targetNode;

        /// <summary>Creates a new instance of a <see cref="GeckoContextMenuEventArgs"/> object.</summary>
        public GeckoContextMenuEventArgs(Point location, ContextMenu contextMenu, string associatedLink, Uri backgroundImageSrc, Uri imageSrc, GeckoNode targetNode)
        {
            Location = location;
            _contextMenu = contextMenu;
            AssociatedLink = associatedLink;
            BackgroundImageSrc = backgroundImageSrc;
            ImageSrc = imageSrc;
            _targetNode = targetNode;
        }

        /// <summary>
        /// Gets or sets the context menu to be displayed.  Set this property to null to disable
        /// the context menu.
        /// </summary>
        public ContextMenu ContextMenu
        {
            get { return _contextMenu; }
            set { _contextMenu = value; }
        }
        ContextMenu _contextMenu;

        public GeckoNode TargetNode
        {
            get { return _targetNode; }
        }

    }

}
