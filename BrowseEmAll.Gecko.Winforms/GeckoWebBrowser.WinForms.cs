﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using BrowseEmAll.Gecko.Core.Listeners;
using BrowseEmAll.Gecko.Core.Windows;
using BrowseEmAll.Gecko.Core.Interop;
using BrowseEmAll.Gecko.Core;

// PLZ keep all Windows Forms related code here
namespace BrowseEmAll.Gecko.Winforms
{
	partial class GeckoWebBrowser
		: Control
	{
		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override Color BackColor
		{
			get { return base.BackColor; }
			set { base.BackColor = value; }
		}

		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override Image BackgroundImage
		{
			get { return base.BackgroundImage; }
			set { base.BackgroundImage = value; }
		}

		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override ImageLayout BackgroundImageLayout
		{
			get { return base.BackgroundImageLayout; }
			set { base.BackgroundImageLayout = value; }
		}

		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override Font Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		[Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public override string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

		protected override void OnHandleCreated( EventArgs e )
		{
			if ( !this.DesignMode )
			{
				Xpcom.Initialize();
				WindowCreator.Register();

				WebBrowser = Xpcom.CreateInstance<nsIWebBrowser>(Contracts.WebBrowser);
				WebBrowserFocus = ( nsIWebBrowserFocus ) WebBrowser;
				BaseWindow = ( nsIBaseWindow ) WebBrowser;
				WebNav = ( nsIWebNavigation ) WebBrowser;

				WebBrowser.SetContainerWindowAttribute( this );

                BaseWindow.InitWindow( this.Handle, IntPtr.Zero, 0, 0, this.Width, this.Height );

				
				BaseWindow.Create();

				Guid nsIWebProgressListenerGUID = typeof (nsIWebProgressListener).GUID;
				Guid nsIWebProgressListener2GUID = typeof (nsIWebProgressListener2).GUID;
				WebBrowser.AddWebBrowserListener( this.GetWeakReference(), ref nsIWebProgressListenerGUID );
				WebBrowser.AddWebBrowserListener( this.GetWeakReference(), ref nsIWebProgressListener2GUID );

				{
					var domWindow = WebBrowser.GetContentDOMWindowAttribute();
					Marshal.ReleaseComObject(domWindow);
				}

				// history
				{
					var sessionHistory = WebNav.GetSessionHistoryAttribute();
					if ( sessionHistory != null ) sessionHistory.AddSHistoryListener( this );
                }

            }

			base.OnHandleCreated( e );
		}

		protected override void OnHandleDestroyed( EventArgs e )
		{
			if (BaseWindow != null)
			{
				this.Stop();

				nsIDocShell docShell = Xpcom.QueryInterface<nsIDocShell>(BaseWindow);
				if (docShell != null && !docShell.IsBeingDestroyed())
				{
					try
					{
						var window = Xpcom.QueryInterface<nsIDOMWindow>(docShell);
						if (window != null)
						{
							try
							{
								//if (!window.GetClosedAttribute()) window.Close();
							}
							finally
							{
								Xpcom.FreeComObject(ref window);
							}
						}
					}
					finally
					{
						Xpcom.FreeComObject(ref docShell);
					}
				}
				
				BaseWindow.Destroy();

				Xpcom.FreeComObject(ref CommandParams);

				var webBrowserFocus = this.WebBrowserFocus;
				this.WebBrowserFocus = null;
				Xpcom.FreeComObject(ref webBrowserFocus);
				Xpcom.FreeComObject(ref WebNav);
				Xpcom.FreeComObject(ref BaseWindow);
				Xpcom.FreeComObject(ref WebBrowser);
			}

			base.OnHandleDestroyed( e );
		}

		protected override void OnEnter( EventArgs e )
		{
			if ( WebBrowserFocus != null )
				WebBrowserFocus.Activate();
			base.OnEnter( e );
		}

		protected override void OnLeave( EventArgs e )
		{
			if ( WebBrowserFocus != null && !IsBusy )
				WebBrowserFocus.Deactivate();
			base.OnLeave( e );
		}

		protected override void OnSizeChanged( EventArgs e )
		{
			if ( BaseWindow != null )
			{
				BaseWindow.SetPositionAndSize( 0, 0, ClientSize.Width != 0 ? ClientSize.Width : 1, ClientSize.Height != 0 ? ClientSize.Height : 1, true );
			}

			base.OnSizeChanged( e );
		}

		protected override void WndProc( ref Message m )
		{
			const int WM_GETDLGCODE = 0x87;
			const int DLGC_WANTALLKEYS = 0x4;
			const int WM_MOUSEACTIVATE = 0x21;
			const int MA_ACTIVATE = 0x1;
			const int WM_IME_SETCONTEXT = 0x0281;
			const int WM_PAINT = 0x000F;
			const int WM_SETFOCUS = 0x0007;


			const int ISC_SHOWUICOMPOSITIONWINDOW = unchecked ((int)0x80000000);
			if ( !DesignMode )
			{
				IntPtr focus;
				switch ( m.Msg )
				{
					case WM_GETDLGCODE:
						m.Result = ( IntPtr ) DLGC_WANTALLKEYS;
						return;
					case WM_SETFOCUS:
						break;
					case WM_MOUSEACTIVATE:
						if ( Xpcom.IsWindows )
						{
							m.Result = ( IntPtr ) MA_ACTIVATE;
							focus = User32.GetFocus();
							// Console.WriteLine( "focus {0:X8}, Handle {1:X8}", focus.ToInt32(), Handle.ToInt32() );
							if ( !IsSubWindow( Handle, focus ) )
							{
							//	var str = string.Format( "+WM_MOUSEACTIVATE {0:X8} lastfocus", focus.ToInt32() );
							//	System.Diagnostics.Debug.WriteLine( str );
								Console.WriteLine("Activating");
								//if (WebBrowserFocus != null)
								//	WebBrowserFocus.Activate();
								//if (Window != null)
								//	Services.WindowWatcher.ActiveWindow = Window;								
							}
							else
							{
							//	var str = string.Format( "-WM_MOUSEACTIVATE {0:X8} lastfocus", focus.ToInt32() );
							//	System.Diagnostics.Debug.WriteLine( str );
							}
							if (Window != null && !Window.Equals(Core.Services.WindowWatcher.ActiveWindow) )
							{
								if (WebBrowserFocus != null)
									WebBrowserFocus.Activate();
								if (Window != null)
                                    Core.Services.WindowWatcher.ActiveWindow = Window;
                            }
							return;
						}
						return;
						
					//http://msdn.microsoft.com/en-US/library/windows/desktop/dd374142%28v=vs.85%29.aspx
					case WM_IME_SETCONTEXT:
						focus = User32.GetFocus();
						if ( User32.IsChild( Handle, focus ) )
						{
							break;
						}

						if ( WebBrowserFocus != null )
						{
						//	var str = string.Format( "WM_IME_SETCONTEXT {0} {1} {2} (focus on {3})", m.HWnd.ToString( "X8" ), m.WParam, m.LParam.ToString( "X8" ), focus.ToString( "X8" ) );
						//	System.Diagnostics.Debug.WriteLine( str );


							var param = m.LParam.ToInt64();
							if ( ( param & ISC_SHOWUICOMPOSITIONWINDOW ) != 0 )
							{

							}
							if ( m.WParam == IntPtr.Zero )
							{
								// zero
								RemoveInputFocus();
								WebBrowserFocus.Deactivate();
							}
							else
							{
								// non-zero (1)
								WebBrowserFocus.Activate();
								SetInputFocus();
							}
							return;
						}

						break;
					case WM_PAINT:
						break;
				}
			}
			
			// Firefox 17+ can crash when handing this windows message so we just ignore it.
			if (m.Msg == 0x128 /*WM_UPDATEUISTATE*/)
				return;

			base.WndProc( ref m );
		}

		private bool IsSubWindow( IntPtr window, IntPtr candidate )
		{
			// search parent until desktop (flash window is owned by other process)
			while ( candidate!=IntPtr.Zero )
			{
				candidate = User32.GetParent( candidate );
				if ( window == candidate )
				{
					return true;
				}
			}
			return false;
		}

		protected override void OnPaint( PaintEventArgs e )
		{
			if ( this.DesignMode )
			{
				string versionString =
					( ( AssemblyFileVersionAttribute )
					  Attribute.GetCustomAttribute( GetType().Assembly, typeof (AssemblyFileVersionAttribute) ) ).Version;

				using (
					Brush brush = new System.Drawing.Drawing2D.HatchBrush( System.Drawing.Drawing2D.HatchStyle.SolidDiamond,
					                                                       Color.FromArgb( 240, 240, 240 ), Color.White ) )
					e.Graphics.FillRectangle( brush, this.ClientRectangle );

				e.Graphics.DrawString(
					string.Format( "BrowseEmAll.Gecko v{0}\r\n" + "http://www.browseemall.com/CoreAPI/Gecko", versionString ),
					SystemFonts.MessageBoxFont,
					Brushes.Black,
					new RectangleF( 2, 2, this.Width - 4, this.Height - 4 ) );
				e.Graphics.DrawRectangle( SystemPens.ControlDark, 0, 0, Width - 1, Height - 1 );
			}
			base.OnPaint( e );
		}

        protected override void OnPrint(PaintEventArgs e)
        {
            base.OnPrint(e);
            if (!this.DesignMode)
			{
				ImageCreator creator = new ImageCreator(this);
				byte[] mBytes = creator.CanvasGetPngImage((uint)0, (uint)0, (uint)this.Width, (uint)this.Height);
				using (Image image = Image.FromStream(new System.IO.MemoryStream(mBytes)))
				{
					e.Graphics.DrawImage(image, 0.0f, 0.0f);
				}
			}
        }

		/// <summary>
		/// This method is called by gecko when showing a print dialog; window handle
		/// returned here is the dialog's owner. If model print dialog is not required, just returns NULL.
		/// 
		/// TODO: Gecko destroys the window returned after the dialog is dismissed, so we can't return this.Handle, we
		/// create and return a temp window instead. This may be a bug of gecko.
		/// 
		/// </summary>
		/// <returns></returns>
		IntPtr nsIEmbeddingSiteWindow.GetSiteWindowAttribute()
		{
			const string name = "TempSubWindow";
			var temp = Controls[name];
			if (temp == null)
			{
				temp = new Control()
				{
					Top = -10,
					Left = -10,
					Width = 1,
					Height = 1,
					Name = name
				};
				temp.HandleDestroyed += (s, e) => Controls.Remove(temp);
				Controls.Add(temp);
			}
			return temp.Handle;
		}

		/// <summary>
		/// A window to contain a tool tip.
		/// </summary>
		private class ToolTipWindow : ToolTip
		{
		}

		public void ForceRedraw()
		{
			BaseWindow.Repaint( true );
		}

		/// <summary>
		/// UI platform independent call function from UI thread
		/// </summary>
		/// <param name="action"></param>
		public void UserInterfaceThreadInvoke( Action action )
		{
			if ( InvokeRequired )
			{
				Invoke( new Action( () => SafeAction( action ) ) );
			}
			else
			{
				SafeAction( action );
			}
		}

		/// <summary>
		/// Exception handler for action
		/// </summary>
		/// <param name="action"></param>
		private void SafeAction( Action action )
		{
			try
			{
				action();
			}
			catch ( Exception)
			{
				System.Diagnostics.Debug.WriteLine( string.Format( "Invoking exception" ) );
			}
		}

		/// <summary>
		/// UI platform independent call function from UI thread
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		public T UserInterfaceThreadInvoke<T>( Func<T> func )
		{
			if ( InvokeRequired )
			{
				return ( T ) Invoke( new Func<T>( () => SafeFunc( func ) ) );
			}
			return SafeFunc( func );
		}

		/// <summary>
		/// exception handler for function
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		private T SafeFunc<T>( Func<T> func )
		{
			T ret = default( T );
			try
			{
				ret = func();
			}
			catch ( Exception )
			{
				System.Diagnostics.Debug.WriteLine( string.Format( "Invoking exception" ) );
			}
			return ret;
		}
	}
}
