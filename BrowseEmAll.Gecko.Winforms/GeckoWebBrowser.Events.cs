﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BrowseEmAll.Gecko.Core.Events;
using BrowseEmAll.Gecko.Core.Interop;
using BrowseEmAll.Gecko.Core.Net;
using System.Collections.Generic;
using BrowseEmAll.Gecko.Core;

namespace BrowseEmAll.Gecko.Winforms
{
	partial class GeckoWebBrowser
	{
        // Events similar to the WebBrowser control

        /// <summary>
        /// Occurs when the value of the <see cref="CanGoBack"/> property is changed.
        /// </summary>
        public event EventHandler<EventArgs> CanGoBackChanged;
        protected virtual void OnCanGoBackChanged(EventArgs e)
        {
            if (CanGoBackChanged != null)
                CanGoBackChanged(this, e);
        }

        /// <summary>
        /// Occurs when the value of the <see cref="CanGoForward"/> property is changed.
        /// </summary>
        public event EventHandler<EventArgs> CanGoForwardChanged;
        protected virtual void OnCanGoForwardChanged(EventArgs e)
        {
            if (CanGoForwardChanged != null)
                CanGoForwardChanged(this, e);
        }

        /// <summary>
        /// Occurs after the browser has finished parsing a new page and updated the <see cref="Document"/> property.
        /// </summary>
        public event EventHandler<GeckoDocumentCompletedEventArgs> DocumentCompleted;
        protected virtual void OnDocumentCompleted(GeckoDocumentCompletedEventArgs e)
        {
            if (DocumentCompleted != null)
                DocumentCompleted(this, e);
        }

        /// <summary>
        /// Occurs when the value of the <see cref="DocumentTitle"/> property is changed.
        /// </summary>
        public event EventHandler<EventArgs> DocumentTitleChanged;
        protected virtual void OnDocumentTitleChanged(EventArgs e)
        {
            if (DocumentTitleChanged != null)
                DocumentTitleChanged(this, e);
        }

        // EncryptionLevelChanged

        /// <summary>
        /// Occurs after the navigation is retargeted.
        /// E.g. the content-type can't be handled by browser or plugins, or the content is supposed to be downloaded.
        /// </summary>
        public event EventHandler<GeckoFileDownloadEventArgs> FileDownload;
        protected virtual void OnFileDownload(GeckoFileDownloadEventArgs e)
        {
            if (FileDownload != null)
                FileDownload(this, e);
        }

        /// <summary>
        /// Occurs after the browser has navigated to a new page.
        /// </summary>
        public event EventHandler<GeckoNavigatedEventArgs> Navigated;
        protected virtual void OnNavigated(GeckoNavigatedEventArgs e)
        {
            if (Navigated != null)
                Navigated(this, e);
        }

        /// <summary>
        /// Occurs before the browser navigates to a new page.
        /// </summary>
        public event EventHandler<GeckoNavigatingEventArgs> Navigating;
		protected virtual void OnNavigating( GeckoNavigatingEventArgs e )
		{
            if (Navigating != null)
                Navigating(this, e);
		}

        /// <summary>
        /// Occurs when the browser tries to create a new window
        /// </summary>
        public event EventHandler<GeckoCreateWindowEventArgs> NewWindow;
        public virtual void OnNewWindow(GeckoCreateWindowEventArgs e)
        {
            if (NewWindow != null)
                NewWindow(this, e);
        }

        // PaddingChanged

        /// <summary>
        /// Occurs when the control has updated progress information.
        /// </summary>
        public event EventHandler<GeckoProgressEventArgs> ProgressChanged;
        protected virtual void OnProgressChanged(GeckoProgressEventArgs e)
        {
            if (ProgressChanged != null)
                ProgressChanged(this, e);
        }

        /// <summary>
        /// Occurs when the value of the <see cref="StatusText"/> property is changed.
        /// </summary>
        public event EventHandler<EventArgs> StatusTextChanged;
        protected virtual void OnStatusTextChanged(EventArgs e)
        {
            if (StatusTextChanged != null)
                StatusTextChanged(this, e);
        }

        // Additional Events not supported by the WebBrowser control

        /// <summary>
        /// Occurs when navigation to a new page has failed or has been aborted by user.
        /// </summary>
		public event EventHandler<GeckoNavigationErrorEventArgs> NavigationError;
		protected virtual void OnNavigationError( GeckoNavigationErrorEventArgs e )
		{
            if (NavigationError != null)
                NavigationError(this, e);
        }

        /// <summary>
        /// Occurs before the browser redirects to a new page.
        /// </summary>
        public event EventHandler<GeckoRedirectingEventArgs> Redirecting;
		protected virtual void OnRedirecting(GeckoRedirectingEventArgs e)
		{
            if (Redirecting != null)
                Redirecting(this, e);
        }

        /// <summary>
        /// Occurs before the browser navigates to a new frame.
        /// </summary>
        public event EventHandler<GeckoNavigatingEventArgs> FrameNavigating;
		protected virtual void OnFrameNavigating(GeckoNavigatingEventArgs e)
		{
            if (FrameNavigating != null)
                FrameNavigating(this, e);
        }

        /// <summary>
        /// Occurs when a new History entry is added to the browser history
        /// </summary>
        public event EventHandler<GeckoHistoryEventArgs> HistoryNewEntry;
		protected virtual void OnHistoryNewEntry( GeckoHistoryEventArgs e )
		{
            if (HistoryNewEntry != null)
                HistoryNewEntry(this, e);
        }

        /// <summary>
        /// Occurs when the browser navigates back in the history
        /// </summary>
		public event EventHandler<GeckoHistoryEventArgs> HistoryGoBack;
		protected virtual void OnHistoryGoBack( GeckoHistoryEventArgs e )
		{
            if (HistoryGoBack != null)
                HistoryGoBack(this, e);
        }

        /// <summary>
        /// Occurs when the browser navigates forward in the history
        /// </summary>
        public event EventHandler<GeckoHistoryEventArgs> HistoryGoForward;
        protected virtual void OnHistoryGoForward( GeckoHistoryEventArgs e )
		{
            if (HistoryGoForward != null)
                HistoryGoForward(this, e);
        }

        /// <summary>
        /// Occurs when the browser reloads the history
        /// </summary>
        public event EventHandler<GeckoHistoryEventArgs> HistoryReload;
		protected virtual void OnHistoryReload( GeckoHistoryEventArgs e )
		{
            if (HistoryReload != null)
                HistoryReload(this, e);
        }

        /// <summary>
        /// Occurs when the browser navigates to a specific index in the history
        /// </summary>
        public event EventHandler<GeckoHistoryGotoIndexEventArgs> HistoryGotoIndex;
		protected virtual void OnHistoryGotoIndex( GeckoHistoryGotoIndexEventArgs e )
		{
            if (HistoryGotoIndex != null)
                HistoryGotoIndex(this, e);
        }

        /// <summary>
        /// Occurs when the browser deletes the history
        /// </summary>
        public event EventHandler<GeckoHistoryPurgeEventArgs> HistoryPurge;
		protected virtual void OnHistoryPurge( GeckoHistoryPurgeEventArgs e )
		{
            if (HistoryPurge != null)
                HistoryPurge(this, e);
        }

        /// <summary>
        /// Occurs when a browser purges the history
        /// </summary>
        public event EventHandler<GeckoHistoryRepalaceEntryEventArgs> HistoryReplaceEntry;
        protected virtual void OnHistoryReplaceEntry(GeckoHistoryRepalaceEntryEventArgs e)
		{
            if (HistoryReplaceEntry != null)
                HistoryReplaceEntry(this, e);
        }

        /// <summary>
		/// Occurs when the control has updated progress information.
		/// </summary>
		public event EventHandler<GeckoRequestProgressEventArgs> RequestProgressChanged;
		protected virtual void OnRequestProgressChanged(GeckoRequestProgressEventArgs e)
		{
            if (RequestProgressChanged != null)
                RequestProgressChanged(this, e);
        }

        /// <summary>
        /// Occurs when the browser creates a new window 
        /// </summary>
        public event EventHandler<GeckoCreateWindow2EventArgs> CreateWindow2;
		public virtual void OnCreateWindow2( GeckoCreateWindow2EventArgs e )
		{
            if (CreateWindow2 != null)
                CreateWindow2(this, e);
        }

        /// <summary>
        /// Occurs when the browser changes the bounds of a window
        /// </summary>
        public event EventHandler<GeckoWindowSetBoundsEventArgs> WindowSetBounds;
		protected virtual void OnWindowSetBounds( GeckoWindowSetBoundsEventArgs e )
		{
            if (WindowSetBounds != null)
                WindowSetBounds(this, e);
        }

        /// <summary>
        /// Occurs when a browser closes a window
        /// </summary>
        public event EventHandler<EventArgs> WindowClosed;		
		protected virtual void OnWindowClosed(EventArgs e)
		{
            if (WindowClosed != null)
                WindowClosed(this, e);
        }

        /// <summary>
        /// Occurs when the browser is showing the context menu
        /// </summary>
        public event EventHandler<GeckoContextMenuEventArgs> ShowContextMenu;
		protected virtual void OnShowContextMenu( GeckoContextMenuEventArgs e )
		{
            if (ShowContextMenu != null)
                ShowContextMenu(this, e);
        }

        /// <summary>
        /// Occurs when the control has updated progress information.
        /// </summary>
        public event EventHandler<GeckoNSSErrorEventArgs> NSSError;
		protected virtual void OnNSSError(GeckoNSSErrorEventArgs e)
		{
            if (NSSError != null)
                NSSError(this, e);
        }
	}

}
