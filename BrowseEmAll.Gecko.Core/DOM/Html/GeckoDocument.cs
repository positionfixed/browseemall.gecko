using System;
using System.Collections.Generic;
using BrowseEmAll.Gecko.Core.DOM;

namespace BrowseEmAll.Gecko.Core
{
	/// <summary>
	/// Represents a DOM HTML document.
	/// </summary>
	public class GeckoDocument : GeckoDomDocument
	{
		private nsIDOMHTMLDocument _domHtmlDocument;

		internal GeckoDocument(nsIDOMHTMLDocument document) : base(document)
		{
			this._domHtmlDocument = document;
		}
		
		internal static GeckoDocument Create(nsIDOMHTMLDocument document)
		{
			return (document == null) ? null : new GeckoDocument(document);
		}
		
		/// <summary>
		/// Gets the HTML head element.
		/// </summary>
		public GeckoHeadElement Head
		{
			get { return (_domHtmlDocument == null) ? null : GeckoHtmlElement.Create<GeckoHeadElement>((nsIDOMHTMLElement)_domHtmlDocument.GetHeadAttribute()); }
		}

		/// <summary>
		/// Gets the HTML body element.
		/// </summary>
		public GeckoHtmlElement Body
		{
			get { return (_domHtmlDocument == null) ? null : GeckoHtmlElement.Create<GeckoHtmlElement>(_domHtmlDocument.GetBodyAttribute()); }
		}
			
		/// <summary>
		/// Gets the URL of the document.
		/// </summary>
		public Uri Url
		{
			get { return (_domHtmlDocument == null) ? null : new Uri(nsString.Get(_domHtmlDocument.GetURLAttribute)); }
		}
			
		public string Cookie
		{
			get { return (_domHtmlDocument == null) ? null : nsString.Get(_domHtmlDocument.GetCookieAttribute); }
			set { nsString.Set(_domHtmlDocument.SetCookieAttribute, value); }
		}
		
		public string Domain
		{
			get { return (_domHtmlDocument == null) ? null : nsString.Get(_domHtmlDocument.GetDomainAttribute); }
		}
		
	}
}