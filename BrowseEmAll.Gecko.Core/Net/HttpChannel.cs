using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core.Net
{
	public class HttpChannel
		: Channel
	{
		private nsIHttpChannel _httpChannel;

		public HttpChannel( nsIHttpChannel httpChannel )
			: base( httpChannel )
		{
			_httpChannel = httpChannel;
		}

		public static HttpChannel Create( nsIHttpChannel httpChannel )
		{
			return new HttpChannel( httpChannel );
		}

		public nsIHttpChannel Instance
		{
			get
			{
				return _httpChannel;
			}
		}

		public string RequestMethod
		{
			get { return nsString.Get( _httpChannel.GetRequestMethodAttribute ); }
			set { nsString.Set( _httpChannel.SetRequestMethodAttribute, value ); }
		}

		public Uri Referrer
		{
			get { return Xpcom.TranslateUriAttribute( _httpChannel.GetReferrerAttribute ); }
			set { _httpChannel.SetReferrerAttribute( IOService.CreateNsIUri( value.ToString() ) ); }
		}

		public string GetRequestHeader( string header )
		{
			string ret = null;
			try
			{
				ret = nsString.Get( _httpChannel.GetRequestHeader, header );
			}
			catch ( Exception )
			{

			}
			return ret;
		}

		public void SetRequestHeader( string header, string value, bool merge )
		{
			nsString.Set( ( x, y ) => _httpChannel.SetRequestHeader( x, y, merge ), header, value );
		}

		public bool AllowPipelining
		{
			get { return _httpChannel.GetAllowPipeliningAttribute(); }
			set { _httpChannel.SetAllowPipeliningAttribute( value ); }
		}

		public uint RedirectionLimit
		{
			get { return _httpChannel.GetRedirectionLimitAttribute(); }
			set { _httpChannel.SetRedirectionLimitAttribute( value ); }
		}

		public uint ResponseStatus
		{
			get { return _httpChannel.GetResponseStatusAttribute(); }
		}

		public string ResponseStatusText
		{
			get { return nsString.Get( _httpChannel.GetResponseStatusTextAttribute ); }
		}

		public bool RequestSucceeded
		{
			get { return _httpChannel.GetRequestSucceededAttribute(); }
		}

		public string GetResponseHeader( string header )
		{
			return nsString.Get( _httpChannel.GetResponseHeader, header );
		}

		public void SetResponseHeader( string header, string value, bool merge )
		{
			nsString.Set( ( x, y ) => _httpChannel.SetResponseHeader( x, y, merge ), header, value );
		}

		public bool IsNoStoreResponse
		{
			get { return _httpChannel.IsNoStoreResponse(); }
		}

		public bool IsNoCacheResponse
		{
			get { return _httpChannel.IsNoCacheResponse(); }
		}

		#region Casts

		#endregion

		/// <summary>
		/// Creates HttpChannel directly from nsISupports
		/// </summary>
		/// <param name="supports"></param>
		/// <returns></returns>
		public static HttpChannel Create( nsISupports supports )
		{
			//int count = Interop.ComDebug.GetRcwRefCount(supports);

			var channel = Xpcom.QueryInterface<nsIHttpChannel>( supports );
			if ( channel == null ) return null;
			Marshal.ReleaseComObject( channel );
			var ret = new HttpChannel( ( nsIHttpChannel ) supports );

			//var count2=Interop.ComDebug.GetRcwRefCount( supports );

			return ret;
		}
	}
}