using System;
using System.Runtime.InteropServices;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core.Net
{
	public class Channel
		:Request
	{
		private nsIChannel _channel;

		protected Channel(nsIChannel channel)
			:base(channel)
		{
			_channel = channel;
		}

		public static Channel CreateChannel( nsIChannel channel )
		{
			if ( channel is nsIHttpChannel )
			{
				return new HttpChannel( ( nsIHttpChannel ) channel );
			}
			return new Channel( channel );
		}
	}
}