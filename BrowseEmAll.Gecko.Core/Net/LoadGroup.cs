using System.Collections.Generic;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core.Net
{
	public sealed class LoadGroup
		:Request
	{
		internal nsILoadGroup _loadGroup;

		public LoadGroup(nsILoadGroup loadGroup)
			:base(loadGroup)
		{
			_loadGroup = loadGroup;
		}
	}
}