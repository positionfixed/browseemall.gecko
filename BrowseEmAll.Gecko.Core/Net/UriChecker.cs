﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BrowseEmAll.Gecko.Core.Net
{
	public sealed class UriChecker
		: Request
	{
		private nsIURIChecker _checker;

		internal UriChecker( nsIURIChecker checker )
			: base( checker )
		{
			_checker = checker;
		}
	}
}
