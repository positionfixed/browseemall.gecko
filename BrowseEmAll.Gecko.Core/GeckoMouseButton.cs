﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Gecko.Core
{
	public enum GeckoMouseButton
	{
		None = 0,
		Left = 0,
		Middle = 1,
		Right = 2,		
	}
}
