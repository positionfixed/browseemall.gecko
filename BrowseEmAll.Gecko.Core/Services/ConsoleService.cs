using System;
using System.Runtime.InteropServices;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core
{
	public static class ConsoleService
	{
		internal static ComPtr<nsIConsoleService> _consoleService;

		static ConsoleService()
		{
			_consoleService = Xpcom.GetService2<nsIConsoleService>(Contracts.ConsoleService);
		}

		public static void Reset()
		{
			_consoleService.Instance.Reset();
		}
	}
}