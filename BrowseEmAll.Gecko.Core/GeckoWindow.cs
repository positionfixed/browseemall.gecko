using System;
using System.Runtime.InteropServices;
using BrowseEmAll.Gecko.Core.DOM;
using BrowseEmAll.Gecko.Core.Interop;

namespace BrowseEmAll.Gecko.Core
{
	/// <summary>
	/// Represents a DOM window.
	/// </summary>
	public class GeckoWindow
		:IEquatable<GeckoWindow>,IDisposable 
	{
		private ComPtr<nsIDOMWindow> _domWindow;

		#region ctor & dtor
		public GeckoWindow(nsIDOMWindow window)
		{
			//Interop.ComDebug.WriteDebugInfo( window );
			_domWindow = new ComPtr<nsIDOMWindow>( window );
		}

		~GeckoWindow()
		{
			Xpcom.DisposeObject( ref _domWindow );
		}

		public void Dispose()
		{
			Xpcom.DisposeObject( ref _domWindow );
			GC.SuppressFinalize( this );
		}
		#endregion

		/// <summary>
		/// Gets the underlying unmanaged DOM object.
		/// </summary>
		public nsIDOMWindow DomWindow
		{
			get { return _domWindow.Instance; }
		}

		public bool Equals(GeckoWindow other)
		{
			if (ReferenceEquals(this, other)) return true;
			if (ReferenceEquals(other, null)) return false;
			return _domWindow.Instance.GetHashCode() == other._domWindow.Instance.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj)) return true;
			if (ReferenceEquals(obj, null)) return false;
			return _domWindow.Instance.GetHashCode() == ((GeckoWindow)obj)._domWindow.Instance.GetHashCode();
		}

		public override int GetHashCode()
		{
			return _domWindow.Instance.GetHashCode();
		}

	}


}