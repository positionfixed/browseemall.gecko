using System;
using System.Runtime.InteropServices;

namespace BrowseEmAll.Gecko.Core.IO
{
	public sealed class StringInputStream
		:InputStream
	{
		private nsIStringInputStream _stringInputStream;

		internal StringInputStream(nsIStringInputStream stream)
			:base(stream)
		{
			_stringInputStream = stream;
		}

	}
}