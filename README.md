# BrowseEmAll.Gecko #

BrowseEmAll.Gecko is a .NET Winforms control for the [Firefox browser](https://www.mozilla.org/en-US/firefox/new/) or more precisely for the rendering engine (Gecko) and JavaScript engine (Spidermonkey).

This project supports embedding and controlling of the Firefox browser into any .NET application.

## Getting started ##

To integrate BrowseEmAll.Gecko into your application simply follow the below steps:

* Download the latest build of BrowseEmAll.Gecko at the Downloads section
* Download the necessary Firefox build from [Mozilla](https://ftp.mozilla.org/pub/firefox/releases/) **(Make sure that the Firefox version matches the BrowseEmAll.Gecko.Core version)**
* Add a reference for BrowseEmAll.Gecko.Core.dll and BrowseEmAll.Gecko.Winforms.dll to your .NET project
* Extract the Firefox build to any location you choose. **If your application is running in 64bit mode a 64bit build of Firefox is required!**
* Initialize Firefox at the start of your application with 
```
#!c#
Xpcom.Initialize("Path:\To\Your\Firefox\Build\Folder");
```
* Create a new GeckoWebBrowser and add it to your UI
```
#!c#
var browser = new GeckoWebBrowser();
// Add browser to your UI somehow
// panel.Controls.Add(browser);
```

You can find a full example in BrowseEmAll.Gecko.Test in this repository.

## Differences to GeckoFx ##

This project is based on the work done for [GeckoFx](https://bitbucket.org/geckofx|GeckoFX) but aims to work with a normal release build of Firefox. Because if this it offers less functionality than the original GeckoFx control, especially in regards to JavaScript execution.

## Important Methods / Events ##

The most used methods are:

* Navigate(string url)
* GoBack()
* GoForward()
* Stop()
* Reload()
* ExecuteJavaScript(string javascript)
* TakePNGScreenshot()

The most used events are:

* Navigated
* DocumentComplete
* StatusTextChanged
* DocumentTitleChanged
* NSSError

## Licensing ##

This project is licensed under the Mozilla Public License Version.